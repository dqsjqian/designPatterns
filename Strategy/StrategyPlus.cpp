/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

// Define the strategy type
typedef enum StrategyType
{
    StrategyA,
    StrategyB,
    StrategyC
}STRATEGYTYPE;

// The abstract strategy
class Strategy
{
public:
    virtual void AlgorithmInterface() = 0;
    virtual ~Strategy() = 0; // 谢谢hellowei提出的bug，具体可以参见评论
};

Strategy::~Strategy()
{}

class ConcreteStrategyA : public Strategy
{
public:
    void AlgorithmInterface()
    {
        cout << "I am from ConcreteStrategyA." << endl;
    }

    ~ConcreteStrategyA() {}
};

class ConcreteStrategyB : public Strategy
{
public:
    void AlgorithmInterface()
    {
        cout << "I am from ConcreteStrategyB." << endl;
    }

    ~ConcreteStrategyB() {}
};

class ConcreteStrategyC : public Strategy
{
public:
    void AlgorithmInterface()
    {
        cout << "I am from ConcreteStrategyC." << endl;
    }

    ~ConcreteStrategyC() {}
};

class Context
{
public:
    Context(STRATEGYTYPE strategyType)
    {
        switch (strategyType)
        {
        case StrategyA:
            pStrategy = new ConcreteStrategyA;
            break;

        case StrategyB:
            pStrategy = new ConcreteStrategyB;
            break;

        case StrategyC:
            pStrategy = new ConcreteStrategyC;
            break;

        default:
            break;
        }
    }

    ~Context()
    {
        if (pStrategy) delete pStrategy;
    }

    void ContextInterface()
    {
        if (pStrategy)
            pStrategy->AlgorithmInterface();
    }

private:
    Strategy* pStrategy;
};

int main()
{
    Context* pContext = new Context(StrategyA);
    pContext->ContextInterface();

    if (pContext) delete pContext;
}