/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

class Component
{
public:
	virtual void operate() = 0;
};

class concreteComponent : public Component
{
public:
	virtual void operate() override
	{
		cout << "concrete component operate" << endl;
	}
};

class Decorator : public Component
{
public:
	Decorator(Component *com) { _com = com; }
	virtual void operate() override
	{
		if (_com)_com->operate();
	}
private:
	Component *_com;
};

class concreteDecoratorA : public Decorator
{
public:
	concreteDecoratorA(Component* com) : Decorator(com) {}
	virtual void operate() override
	{
		AddedBehavior();
		Decorator::operate();
	}
	void AddedBehavior()
	{
		cout << "add behavior A" << endl;
	}
};

class concreteDecoratorB : public Decorator
{
public:
	concreteDecoratorB(Component* com) : Decorator(com) {}
	virtual void operate() override
	{
		AddedBehavior();
		Decorator::operate();
	}
	void AddedBehavior()
	{
		cout << "add behavior B" << endl;
	}
};

int main()
{
	Component * com = new concreteComponent();
	Decorator * deA = new concreteDecoratorA(com);
	Decorator * deB = new concreteDecoratorB(com);
	Decorator * deC = new concreteDecoratorB(deA);

	com->operate();
	cout << "=============================================" << endl;
	deA->operate();
	cout << "=============================================" << endl;
	deB->operate();
	cout << "=============================================" << endl;
	deC->operate();

	//todo ��Դ�ͷ�
		
	return 0;
}