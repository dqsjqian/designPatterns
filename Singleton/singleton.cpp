/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

class Singleton
{
public:
	static Singleton* instance() { static Singleton _single; return &_single; }
	~Singleton() {}
private:
	Singleton() {};
};

class Func
{
public:
	Func() {}
	~Func() {}
public:
	void func1() { printf("1---%p\n", Singleton::instance()); }
	void func2() { printf("2---%p\n", Singleton::instance()); }
};

int main()
{
	Func *f = new Func();
	f->func1();
	f->func2();

	return 0;
}