/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <thread>
using namespace std;

typedef enum
{
	Type1,
	Type2,
	Type3
}ProductType;

class ProductBase
{
public:
	virtual void show() = 0;
};

class productA : public ProductBase
{
public:
	void show() override
	{
		cout << "product A" << endl;
	}
};

class productB : public ProductBase
{
public:
	void show() override
	{
		cout << "product B" << endl;
	}
};

class Factory
{
public:
	ProductBase * createProduct(ProductType pt);
};

ProductBase * Factory::createProduct(ProductType pt)
{
	switch (pt)
	{
	case Type1:
		return new productA();
	case Type2:
		return new productB();
	default:
		return nullptr;
	}
}

int main()
{
	Factory * a = new Factory();
	ProductBase *p1 = a->createProduct(Type1);
	ProductBase *p2 = a->createProduct(Type2);
	
	p1->show();
	p2->show();

	auto b = std::shared_ptr<Factory>();
	auto p3 = b->createProduct(Type1);
	auto p4 = b->createProduct(Type2);
	auto p5 = std::shared_ptr<ProductBase>(b->createProduct(Type2));

	//auto p6 = std::make_shared<ProductBase>(b->createProduct(Type2));
	//不能这么使用，make_shared涉及到模版参数类的实例化，而虚基类不能实例化！
	//p5中的shared_ptr只是一个函数调用，不涉及类的实例化，所以模版参数可以是虚基类！


	auto p7 = std::make_shared<productA>();

	p3->show();
	p4->show();
	p5->show();
	//p6->show();
	p7->show();

	//todo 资源释放

	return 0;
}
