/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <memory>
using namespace std;

class ProductA
{
public:
	virtual void show() = 0;
};

class ProductA1 :public ProductA
{
public:
	virtual void show() override
	{
		cout << "productA1" << endl;
	}
};

class ProductA2 :public ProductA
{
public:
	virtual void show() override
	{
		cout << "productA2" << endl;
	}
};


class ProductB
{
public:
	virtual void show() = 0;
};

class ProductB1 :public ProductB
{
public:
	virtual void show() override
	{
		cout << "productB1" << endl;
	}
};

class ProductB2 :public ProductB
{
public:
	virtual void show() override
	{
		cout << "productB2" << endl;
	}
};

class Factory
{
public:
	virtual ProductA *createProductA() = 0;
	virtual ProductB *createProductB() = 0;
};

class Factory1 :public Factory
{
public:
	virtual ProductA *createProductA() override
	{
		return new ProductA1();
	}

	virtual ProductB *createProductB() override
	{
		return new ProductB1();
	}
};

class Factory2 :public Factory
{
public:
	virtual ProductA *createProductA() override
	{
		return new ProductA2();
	}

	virtual ProductB *createProductB() override
	{
		return new ProductB2();
	}
};


int main()
{
	Factory *fac1 = new Factory1();
	ProductA *proA1 = fac1->createProductA();
	ProductB *proB1 = fac1->createProductB();
	proA1->show();
	proB1->show();

	Factory *fac2 = new Factory2();
	ProductA *proA2 = fac2->createProductA();
	ProductB *proB2 = fac2->createProductB();
	proA2->show();
	proB2->show();

	//todo ��Դ�ͷ� 

	return 0;
}