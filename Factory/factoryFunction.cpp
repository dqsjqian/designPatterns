/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <memory>
using namespace std;

class Product
{
public:
	virtual void show() = 0;
};

class ProductA : public Product
{
public:
	virtual void show() override
	{
		cout << "product A" << endl;
	}
};

class ProductB : public Product
{
public:
	virtual void show() override
	{
		cout << "product B" << endl;
	}
};

class Factory
{
public:
	virtual Product* createProduct() = 0;
};

class FactoryA : public Factory
{
public:
	virtual Product* createProduct() override
	{
		return new ProductA();
	}
};

class FactoryB : public Factory
{
public:
	virtual Product* createProduct() override
	{
		return new ProductB();
	}
};


int main()
{
	Factory *f1 = new FactoryA();
	Factory *f2 = new FactoryB();
	Product *p1 = f1->createProduct();
	Product *p2 = f2->createProduct();

	p1->show();
	p2->show();

	auto f3 = std::shared_ptr<Factory>(new FactoryA());
	auto f4 = std::shared_ptr<Factory>(new FactoryB());
	auto p3 = std::shared_ptr<Product>(f3->createProduct());
	auto p4 = std::shared_ptr<Product>(f4->createProduct());

	p3->show();
	p4->show();

	return 0;
}