/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <map>
#include <vector>
using namespace std;

typedef struct pointTag
{
	int x, y;
	pointTag() {}
	pointTag(int a, int b) :x(a), y(b) {}

	bool operator <(const pointTag& p)const
	{
		if (x < p.x)return true;
		else if (x == p.x)return y < p.y;
		else return false;
	}
}POINT;

typedef enum colorTag
{
	BLACK,
	WHITE
}COLOR;

class CPiece
{
public:
	CPiece(COLOR color) :_color(color) {}
	COLOR getColor() { return _color; }

	void setPoint(POINT point) { _point = point; }
	POINT getPoint() { return _point; }
private:
	COLOR _color;
	POINT _point;
};

class CGomoku : public CPiece
{
public:
	CGomoku(COLOR color) :CPiece(color) {}
};

class CPieceFactory
{
public:
	CPiece* getPiece(COLOR color)
	{
		CPiece* piece = nullptr;
		if (_cpiece.empty())
		{
			piece = new CPiece(color);
			_cpiece.push_back(piece);
		}
		else
		{
			for (auto& a : _cpiece)
			{
				if (a->getColor() == color)
				{
					piece = a;
					break;
				}
				if (!piece)
				{
					piece = new CGomoku(color);
					_cpiece.push_back(piece);
				}
			}
		}
		return piece;
	}

	~CPieceFactory()
	{
		for (auto& a : _cpiece)
		{
			if (!a)
			{
				delete a;
				a = nullptr;
			}
		}
	}

private:
	vector<CPiece*> _cpiece;
};


class CChessBoard
{
public:
	void draw(CPiece* piece)
	{
		if (piece->getColor())//为1表示白棋，0表示黑棋
			cout << "下了一个白棋：" << piece->getPoint().x << "," << piece->getPoint().y << endl;
		else
			cout << "下了一个黑棋：" << piece->getPoint().x << "," << piece->getPoint().y << endl;

		_piece.push_back(piece);
	}

	void showAllPieces()
	{
		for (auto& a : _piece)
		{
			if (a->getColor())
				cout << a->getPoint().x << "," << a->getPoint().y << " is a white piece" << endl;
			else
				cout << a->getPoint().x << "," << a->getPoint().y << " is a black piece" << endl;
		}
	}

private:
	vector<CPiece*> _piece;
};


int main()
{
	CPieceFactory* cpf = new CPieceFactory();
	CChessBoard* ccb = new CChessBoard();

	CPiece* cp = cpf->getPiece(WHITE);
	cp->setPoint(POINT(2, 3));
	ccb->draw(cp);

	cp = cpf->getPiece(BLACK);
	cp->setPoint(POINT(2, 4));
	ccb->draw(cp);

	cp = cpf->getPiece(WHITE);
	cp->setPoint(POINT(2, 2));
	ccb->draw(cp);

	cp = cpf->getPiece(BLACK);
	cp->setPoint(POINT(3, 4));
	ccb->draw(cp);

	ccb->showAllPieces();

	//todo 资源释放

	return 0;
}
