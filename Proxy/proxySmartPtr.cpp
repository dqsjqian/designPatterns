/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <windows.h>
using namespace std;

#define delete_safe(p) if (p) { delete p; p = nullptr; }

class KRefCount
{
public:
	KRefCount() :m_nCount(0) {}

public:
	unsigned AddRef() { return InterlockedIncrement(&m_nCount); }//原子加，为了线程安全
	unsigned Release() { return InterlockedDecrement(&m_nCount); }//原子减，为了线程安全
	void Reset() { m_nCount = 0; }//重置

private:
	unsigned long m_nCount;
};

template <typename T>
class SmartPtr
{
public:
	SmartPtr(void)
		: m_pData(nullptr)
	{
		m_pReference = new KRefCount();
		m_pReference->AddRef();
	}

	SmartPtr(T* pValue)
		: m_pData(pValue)
	{
		m_pReference = new KRefCount();
		m_pReference->AddRef();
	}

	SmartPtr(const SmartPtr<T>& sp)
		: m_pData(sp.m_pData)
		, m_pReference(sp.m_pReference)
	{
		m_pReference->AddRef();
	}

	~SmartPtr(void)
	{
		if (m_pReference && m_pReference->Release() == 0)
		{
			delete_safe(m_pData);
			delete_safe(m_pReference);
		}
	}

	inline T& operator*()
	{
		return *m_pData;
	}

	inline T* operator->()
	{
		return m_pData;
	}

	SmartPtr<T>& operator=(const SmartPtr<T>& sp)
	{
		if (this != &sp)
		{
			if (m_pReference && m_pReference->Release() == 0)
			{
				delete_safe(m_pData);
				delete_safe(m_pReference);
			}

			m_pData = sp.m_pData;
			m_pReference = sp.m_pReference;
			m_pReference->AddRef();
		}

		return *this;
	}

	SmartPtr<T>& operator=(T* pValue)
	{
		if (m_pReference && m_pReference->Release() == 0)
		{
			delete_safe(m_pData);
			delete_safe(m_pReference);
		}

		m_pData = pValue;
		m_pReference = new KRefCount;
		m_pReference->AddRef();
		return *this;
	}

	T* Get()
	{
		T* ptr = nullptr;
		ptr = m_pData;

		return ptr;
	}

	void Attach(T* pObject)
	{
		if (m_pReference->Release() == 0)
		{
			delete_safe(m_pData);
			delete_safe(m_pReference);
		}

		m_pData = pObject;
		m_pReference = new KRefCount;
		m_pReference->AddRef();
	}

	T* Detach()
	{
		T* ptr = nullptr;

		if (m_pData)
		{
			ptr = m_pData;
			m_pData = nullptr;
			m_pReference->Reset();
		}
		return ptr;
	}

private:
	KRefCount* m_pReference;
	T* m_pData;
};

class CTest
{
public:
	CTest(int b) : a(b) {}
private:
	int a;
};

int main()
{
	SmartPtr<CTest> pSmartPtr1(new CTest(10));
	SmartPtr<CTest> pSmartPtr2(new CTest(20));

	pSmartPtr1 = pSmartPtr2;

	return 0;
}