/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

#define delete_safe(p) if (p) { delete p; p = nullptr;}

class CSubject
{
public:
	CSubject() {};
	virtual ~CSubject() {}

	virtual void Request() = 0;
};

class CRealSubject : public CSubject
{
public:
	CRealSubject() {}
	~CRealSubject() {}

	void Request()
	{
		cout << "CRealSubject Request" << endl;
	}
};

class CProxy : public CSubject
{
public:
	CProxy() : m_pRealSubject(nullptr) {}
	~CProxy()
	{
		delete_safe(m_pRealSubject);
	}

	void Request()
	{
		if (nullptr == m_pRealSubject)
		{
			m_pRealSubject = new CRealSubject();
		}
		cout << "CProxy Request" << endl;
		m_pRealSubject->Request();
	}

private:
	CRealSubject* m_pRealSubject;
};

int main()
{
	CSubject* pSubject = new CProxy();
	pSubject->Request();
	delete_safe(pSubject);
}