/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

class ProtoType
{
public:
	ProtoType() {}
	virtual ~ProtoType() {}
	virtual ProtoType* clone() = 0;
	virtual void show() = 0;
};

class concreteProtoType : public ProtoType
{
public:
	concreteProtoType() : _a(100) {}
	virtual ~concreteProtoType() {}

	concreteProtoType(const concreteProtoType &con)
	{
		_a = con._a;
	}

	virtual ProtoType* clone()
	{
		return new concreteProtoType(*this);
	}

	virtual void show() override
	{
		cout << _a << endl;
	}
private:
	int _a;
};

int main()
{
	ProtoType *p1 = new concreteProtoType();
	ProtoType *p2 = p1->clone();

	p1->show();
	p2->show();

	return 0;
}