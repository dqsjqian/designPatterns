/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

class Implementor
{
public:
	virtual ~Implementor() {}
	virtual void operationImp() = 0;
};

class concreteImplementor : public Implementor
{
public:
	virtual void operationImp() override
	{
		cout << "operationImp" << endl;
	}
};

class Abstraction
{
public:
	virtual ~Abstraction() {}
	virtual void doSomething() = 0;
};

class RefineAbstraction : public Abstraction
{
public:
	RefineAbstraction(Implementor *imp) :_imp(imp) {}
	virtual void doSomething() override
	{
		_imp->operationImp();
	}
private:
	Implementor *_imp;
};

int main()
{
	Implementor *imp = new concreteImplementor();
	Abstraction *ab = new RefineAbstraction(imp);

	ab->doSomething();

	//todo �ڴ��ͷ�

	return 0;
}