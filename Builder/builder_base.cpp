/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Product
{
private:
	vector<string> _v;
public:
	void add(const char* str) { _v.push_back(str); }
	void show()
	{
		for (auto &a : _v)
		{
			cout << a << endl;
		}
	}
};

class Builder
{
public:
	virtual void doSomething() = 0;
	virtual Product* getProduct() = 0;
};

class concreteBuilder : public Builder
{
public:
	concreteBuilder() { _p = new Product(); }
	virtual void doSomething() override
	{
		_p->add("hehe");
	}

	virtual Product* getProduct() override
	{
		return _p;
	}

private:
	Product *_p = nullptr;
};

class Director
{
public:
	Director() = delete;
	Director(Builder *b) :_b(b) {}
	void createBuilder()
	{
		_b->doSomething();
	}
private:
	Builder *_b = nullptr;
};

int main()
{
	Builder *b = new concreteBuilder();
	Director *d = new Director(b);
	d->createBuilder();

	Product *p = b->getProduct();
	p->show();
}