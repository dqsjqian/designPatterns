/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

typedef enum man_tag
{
	FAT,
	THIN,
	NORMAL
}manType;

class Man
{
public:
	Man(manType m) : _m(m) {}
	void setHead() { cout << "Man setHead" << endl; }
	void setBody() { cout << "Man setBody" << endl; }

	void showMan()
	{
		switch (_m)
		{
		case manType::FAT:
			cout << "fat man" << endl;
			break;
		case manType::THIN:
			cout << "thin man" << endl;
			break;
		case manType::NORMAL:
			cout << "normal man" << endl;
			break;
		default:
			cout << "error man" << endl;
			break;
		}
	}
private:
	manType _m;
};

class Builder
{
public:
	virtual void buildHead() = 0;
	virtual void BuildBody() = 0;
	virtual Man *getMan() = 0;
};

class FatManBuilder : public Builder
{
public:
	explicit FatManBuilder() { _m = new Man(FAT); }
	~FatManBuilder() { if (_m) { delete _m; _m = nullptr; } }

	virtual void buildHead() override
	{
		_m->setHead();
	}

	virtual void BuildBody() override
	{
		_m->setBody();
	}

	virtual Man *getMan() override
	{
		return _m;
	}

private:
	Man * _m = nullptr;
};

class ThinManBuilder : public Builder
{
public:
	explicit ThinManBuilder() { _m = new Man(THIN); }
	~ThinManBuilder() { if (_m) { delete _m; _m = nullptr; } }

	virtual void buildHead() override
	{
		_m->setHead();
	}

	virtual void BuildBody() override
	{
		_m->setBody();
	}

	virtual Man *getMan() override
	{
		return _m;
	}

private:
	Man * _m = nullptr;
};

class Director
{
public:
	Director() = delete;
	Director(Builder *b) :_b(b) {}
	~Director() { if (_b) { delete _b; _b = nullptr; } }

public:
	void createMan()
	{
		_b->buildHead();
		_b->BuildBody();
	}

	Man *getMan()
	{
		return _b->getMan();
	}

private:
	Builder *_b = nullptr;
};

int main()
{
	Builder* b = new FatManBuilder();
	Director* d = new Director(b);
	d->createMan();

	Man* m = b->getMan();
	m->showMan();

	//����
	d->getMan()->showMan();

	return 0;
}



















