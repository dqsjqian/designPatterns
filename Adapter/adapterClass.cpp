/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
using namespace std;

class Target
{
public:
	virtual void request()
	{
		cout << "target request" << endl;
	}
};

class Adaptee
{
public:
	void adapteeRequest()
	{
		cout << "adaptee request" << endl;
	}
};

class Adapter : public Target, Adaptee
{
public:
	virtual void request() override
	{
		Adaptee::adapteeRequest();
	}
};

int main()
{
	Target *a = new Adapter();
	a->request();

	//todo �ڴ��ͷ�

	return 0;
}






