/*
 *  Copyright 2020, dqsjqian(Mr.Zhang).  All right reserved.
 *
 *  THIS IS UNPUBLISHED PROPRIETARY SOURCE CODE OF dqsjqian(Mr.Zhang).
 *  LTD.  THE CONTENTS OF THIS FILE MAY NOT BE DISCLOSED TO THIRD
 *  PARTIES, COPIED OR DUPLICATED IN ANY FORM, IN WHOLE OR IN PART,
 *  WITHOUT THE PRIOR WRITTEN PERMISSION OF dqsjqian(Mr.Zhang).
 *
 *
 *  Edit History:
 *
 *    2020-01-12 - Created by dqsjqian(Mr.Zhang) dqsjqian@163.com
 *	Blog: https://blog.csdn.net/dqsjqian
 *	Gitee: https://gitee.com/dqsjqian
 */

#include <iostream>
#include <string>
#include <vector>
#include <memory>
using namespace std;

template <typename T>
class Component
{
public:
	Component(const T& name) :m_name(name) {}
	virtual ~Component() {}
	virtual void operate() = 0;
	virtual void add(Component**) = 0;
	virtual void remove(Component**) = 0;
	virtual void show() = 0;
	virtual Component* getchild(size_t) = 0;
	virtual T getName() { return m_name; }
protected:
	T m_name;
};

template <typename T>
class Leaf : public Component<T>
{
public:
	Leaf(const T& name) : Component(name) {}
	virtual ~Leaf() {}
	virtual void operate() override { cout << "I'm" + m_name << endl; }
	virtual void add(Component**) override {}
	virtual void remove(Component**) override {}
	virtual void show() override {}
	virtual Component* getchild(size_t) override { return nullptr; }
};

template <typename T>
class Composite : public Component<T>
{
public:
	Composite(const T& name) : Component(name) {}
	virtual ~Composite()
	{
		auto it = _vec.begin();
		while (it != _vec.end())
		{
			if (**it)
			{
				delete **it;
				**it = nullptr;
				it = _vec.erase(it);
			}
			else it++;
		}
	}
	virtual void operate() override { cout << "I'm" + m_name << endl; }
	virtual void add(Component** c) override
	{
		_vec.push_back(c);
	}
	virtual void remove(Component** c) override
	{
		auto it = _vec.begin();
		for (; it != _vec.end(); ++it)
		{
			if ((*(*it))->getName() == (*c)->getName())
			{
				if (**it)
				{
					delete **it;
					**it = nullptr;
				}
				*c = nullptr;
				_vec.erase(it);
				break;
			}
		}
	}
	virtual void show() override
	{
		for (auto a : _vec)
		{
			if (*a)cout << (*a)->getName() << endl;
		}
	}

	virtual Component* getchild(size_t index) override
	{
		if (index > _vec.size())return nullptr;
		else return *(_vec[index - 1]);
	}

private:
	std::vector<Component**> _vec;
};

int main()
{
	cout << "-------------总公司结构-------------" << endl;
	Component<std::string> *base = new Composite<std::string>("总公司");
	Component<std::string> *base_hr = new Leaf<std::string>("总公司hr");
	Component<std::string> *base_pd = new Leaf<std::string>("总公司pd");
	Component<std::string> *sub_SH = new Composite<std::string>("上海分公司");
	Component<std::string> *sub_WH = new Composite<std::string>("武汉分公司");
	base->add(&base_hr);
	base->add(&base_pd);
	base->add(&sub_SH);
	base->add(&sub_WH);
	base->show();

	base->getchild(1)->getName();

	cout << "-------------上海分公司结构-------------" << endl;
	Component<std::string> *sub_SH_hr = new Leaf<std::string>("上海分公司hr");
	Component<std::string> *sub_SH_pd = new Leaf<std::string>("上海分公司pd");
	sub_SH->add(&sub_SH_hr);
	sub_SH->add(&sub_SH_pd);
	sub_SH->show();

	cout << "-----------裁掉上海分公司hr部门，此时的上海公司结构----------" << endl;
	//裁掉上海分公司hr部门
	sub_SH->remove(&sub_SH_hr);
	sub_SH->show();

	cout << "-----------裁掉整个上海分公司，此时的总公司结构----------" << endl;
	//裁掉整个上海分公司
	base->remove(&sub_SH);
	base->show();

	cout << "-------------此时的上海分公司------------" << endl;
	if (sub_SH)
		sub_SH->show();

	cout << "-------------裁掉总公司hr，此时的总公司------------" << endl;
	base->remove(&base_hr);
	base->show();

	//todo 资源释放

	return 0;
}